import $ from "jquery";

// interfaces
interface SaveData {
	version: number;
	numBerries: number;
	saveTime: number;
	nodes: {
		id: number;
		numOwned: number;
	}[];
}

interface ResourceNode {
	id: number;
	name: string;
	costBase: number;
	costCoef: number;
	power: number;
}

// data
const resourceNodes: ResourceNode[] = [
	{
		id: 0,
		name: "auto-picker",
		costBase: 20,
		costCoef: 1.1,
		power: 1
	},
	{
		id: 1,
		name: "advanced auto-picker",
		costBase: 200,
		costCoef: 1.25,
		power: 10
	},
	{
		id: 2,
		name: "kitten army",
		costBase: 4000,
		costCoef: 1.4,
		power: 80
	},
	{
		id: 3,
		name: "jam tornado",
		costBase: 200000,
		costCoef: 1.5,
		power: 500
	},
	{
		id: 4,
		name: "Lola",
		costBase: 1000000,
		costCoef: 1.6,
		power: 2000
	}
];

// elements
const numBerriesElem = $("#numBerriesValue");
const productionElem = $("#production");

//save data
let saveData: SaveData;

function buyNode(index: number) {
	const nodeDef = resourceNodes[index];
	let saveNode = saveData.nodes.find(n => n.id === nodeDef.id);
	if (saveNode === undefined) {
		saveNode = unlockNode(nodeDef.id);
	}

	const nodeCostNext = costNext(nodeDef.costBase, nodeDef.costCoef, saveNode.numOwned);
	if (saveData.numBerries >= nodeCostNext) {
		saveData.numBerries -= nodeCostNext;
		saveNode.numOwned++;
	}

	if (index < resourceNodes.length - 1) {
		unlockNode(resourceNodes[index + 1].id);
	}

	render();
}

function unlockNode(id: number) {
	let node = saveData.nodes.find(n => n.id === id);
	if (!node) {
		node = {
			id: id,
			numOwned: 0
		};
		saveData.nodes.push(node);
	}
	return node;
}

function costNext(base: number, coef: number, owned: number) {
	return Math.round(base * Math.pow(coef, owned));
}

function render() {
	let productionVal = 0;
	resourceNodes.forEach(nodeDef => {
		let node = saveData.nodes.find(n => n.id === nodeDef.id);
		let numOwned = 0;
		if (node != null) {
			numOwned = node.numOwned;
		}
		const nodeCostNext = costNext(nodeDef.costBase, nodeDef.costCoef, numOwned);
		const elem = $(`#buyNode${nodeDef.id}`);
		elem.text(`${numOwned > 0 ? `(x${numOwned.toLocaleString()}) ` : ""}Buy ${nodeDef.name} for ${nodeCostNext.toLocaleString()}🍓`);

		if (node == null) {
			elem.css({ visibility: "collapse" });
		} else if (nodeCostNext > saveData.numBerries) {
			elem.css({ opacity: 0.5, visibility: "visible" });
		} else {
			elem.css({ opacity: 1, visibility: "visible" });
		}

		productionVal += nodeDef.power * numOwned;
	});

	numBerriesElem.text(saveData.numBerries.toLocaleString());
	productionElem.text(`${productionVal.toLocaleString()} 🍓 per second`);
}

function update(delta: number) {
	saveData.nodes.forEach(node => {
		const nodeDef = resourceNodes.find(n => n.id === node.id);
		saveData.numBerries += node.numOwned * nodeDef.power * delta;
	});
	render();
}

function createSaveData() : SaveData {
	return {
		version: 1,
		numBerries: 0,
		saveTime: Date.now(),
		nodes: [
			{
				id: resourceNodes[0].id,
				numOwned: 0
			}
		]
	}
}

function save() {
	saveData.saveTime = Date.now();
	window.localStorage.setItem("saveData", JSON.stringify(saveData));
}

function load(): SaveData {
	const data = window.localStorage.getItem("saveData");
	if (data == null) return null;
	return JSON.parse(data);
}

function resetSave() {
	if (confirm("Are you sure you want to reset your save data?")) {
		saveData = createSaveData();
		render();
		save();
	}
}

$(() => {
	$("#resetButton").on("click", resetSave);

	//get save data
	saveData = load();
	if (saveData != null) {
		// load save
		const secondsPast = Math.round((Date.now() - saveData.saveTime) / 1000);
		update(secondsPast);
	} else {
		// new game save
		saveData = createSaveData();
	}

	// create all resource buttons
	const statsElem = $("#stats");
	resourceNodes.forEach((node, index) => {
		const elem = $(`<div class="btn buyNode" id="buyNode${index}"></div>`);
		elem.on("click", () => {
			buyNode(index);
		});
		statsElem.append(elem);
	});

	$("#strawberry").on("click touch", (e) => {
		saveData.numBerries++;
		render();
	});

	// setup particles
	if (document.body.animate) {
		$("#strawberry").on("click touch", pop);
	}

	render();
	setInterval(() => update(1), 1000);
	setInterval(save, 3000);
});



// Source https://css-tricks.com/playing-with-particles-using-the-web-animations-api/
function pop(e) {
	for (let i = 0; i < 3; i++) {
		createParticle(e.clientX, e.clientY);
	}
}

function createParticle(x, y) {
	const particle = document.createElement('particle');
	document.body.appendChild(particle);

	const size = 20;
	particle.style.width = `${size}px`;
	particle.style.height = `${size}px`;
	particle.innerHTML = "🍓";

	// Generate a random x & y destination within a distance of 75px from the mouse
	const destinationX = x + (Math.random() - 0.5) * 2 * 150;
	const destinationY = y + (Math.random() - 0.5) * 2 * 150;

	// Store the animation in a variable because we will need it later
	const animation = particle.animate([
		{
			// Set the origin position of the particle
			// We offset the particle with half its size to center it around the mouse
			transform: `translate(${x - (size / 2)}px, ${y - (size / 2)}px)`,
			opacity: 1
		},
		{
			// We define the final coordinates as the second keyframe
			transform: `translate(${destinationX}px, ${destinationY}px)`,
			opacity: 0
		}
	], {
		// Set a random duration from 500 to 1500ms
		duration: 500 + Math.random() * 1000,
		easing: 'cubic-bezier(0, .9, .57, 1)',
		// Delay every particle with a random value from 0ms to 200ms
		delay: Math.random() * 200
	});

	animation.onfinish = () => {
		particle.remove();
	};
}