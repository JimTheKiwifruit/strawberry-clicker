# Strawberry Clicker 🍓

A silly, bare-bones clicker game about collecting strawberries.

## Running Locally

```
npm install
npm run test
```
